"""
Classes implementing an API for tracing source files unpacked by
bb.fetch2.Fetch.unpack back to their respective upstream SRC_URI entries, for
software composition analysis, license compliance and detailed SBOM generation
purposes.

For the tracing of unpacked URLs, a tree data structure is being used to trace
unpacked URLs in order to handle GitSM and NpmShrinkWrap fetchers, that expand
SRC_URI entries by adding implicit URLs and by recursively calling Fetch.unpack
from new (nested) Fetch instances.

For the tracing of unpacked files, a file index is being used to detect changes
(added, modified, removed files) in the unpackdir. We update the file index for
each unpack process, except for GitSM and NpmShrinkWrap expanded/implicit URLs,
which need to be handled only at the end of the main URL's unpack process.
To do so, we need to calculate the destdir for each GitSM and NpmShrinkWrap
expanded/implicit URL, in order to update the file index separately for each
URL.[^1][^2]

Finally, we also collect dependency information for each git submodule and npm
module, because such information is also needed for software composition
analysis, license compliance and SBOM generation purposes.

-----------

[^1]: GitSM fetcher recursively creates nested Fetch instances in case of nested
git submodules, but it checks out only the main/top repository, while all
(nested) submodules are just bare-cloned through nested Fetch.unpack calls, and
checked out all at once only at the end, through a single command call within
the main repo's dir (git submodule update --recursive --no-fetch). This means
that when nested Fetch.unpack calls are made, the files we need to trace are not
there yet because submodules are not checked out at that point. Therefore we
need to update the file index for each submodule only at the end of the main
repo's unpack process, and to be able to do so, we need to calculate each
submodule checkout dir to update the index only for that dir; this is done by
using the modpath attribute of the GitSM urldata.

[^2]: NpmShrinkWrap fetcher does not create multiple nested Fetch instances as
GitSM does, but it unpacks all npm modules (including recursive dependencies)
through a single nested Fetch.unpack call. However, this is true only for "auto"
(i.e. git) dependencies, while "manual" dependencies (which means most
dependencies) are unpacked directly by NpmShrinkWrap fetcher without calling
Fetch.unpack, so we need to update the file index for each "manual" dependency
at the end of the unpack process for the main URL (which in the case of
NpmShrinkWrap is not a module but a npm-shrinkwrap.json file). However, to avoid
using different code paths to handle "auto" and "manual" dependencies, we do the
same also for "auto" dependencies; in this way we use the same code path to
process all modules, both in GitSM and in NpmShrinkWrap. As in GitSM, we need
to calculate each npm module's destdir to update the index only for that dir;
this is done by using "destsuffix" and "name" found in urldata.dep.
"""

# Copyright (C) 2023 Alberto Pianon <pianon@array.eu>
#
# SPDX-License-Identifier: GPL-2.0-only

import os
import re
import json
import hashlib

import bb.fetch2
import bb.utils

from .file_index import FileIndex, simple_fileindex
from .utils import Cache, git_data, git_status, \
    get_layers, get_layer, get_unexpanded_src_uri, get_clean_src_uri

class UrlNode:
    """
    UrlNode represents a node of the URL tree described in this python
    module's help text. It keeps track of the unpacking status of the URL and of
    its children, to check consistency of the API calls made by the core
    bb.fetch2.Fetch.unpack method and by the fetcher-specific unpack methods.

    Bad bbappends may introduce duplicate URIs in recipes, causing Fetch.unpack
    to be called multiple times for the same URI. So we cannot use URI as a
    unique identifier of UrlNode; we use a numeric index (idx) instead.
    """

    CREATE = 0
    START = 1
    UNPACK = 2
    FINISH = 3

    status2str = ["CREATE", "START", "UNPACK", "FINISH"]

    def __init__(self, idx, url, urldata, d, parent):

        self.idx = idx
        self.url = url
        self.urldata = urldata
        self.d = d
        self.parent = parent
        self.children = []

        self.destdir = None
        #self.dep_of_idx = None
        self.dep_of_url = None
        self.dep_of_destdir = None
        self.is_unpacked_archive = False
        self.files = None

        self.status = UrlNode.CREATE

    def _set_status(self, status):
        """
        set the status of the UrlNode and check for consistency with the current
        status
        """
        if status == self.status + 1 and UrlNode.CREATE <= status <= UrlNode.FINISH:
            self.status += 1
        else:
            raise UnpackTracerError(
                "Tracer API call inconsistency: status of UrlNode '%s-%s' is"
                " %s, cannot change it to %s" % (
                    self.idx, self.url,
                    self.status2str[self.status], self.status2str[status])
            )

    def start(self):
        """start the UrlNode"""
        self._set_status(UrlNode.START)

    def unpack(self, unpack_type, destdir):
        """set the destdir of the UrlNode and process the unpack type"""
        self._set_status(UrlNode.UNPACK)
        if unpack_type == "git-bareclone":
            return
        self.destdir = destdir.rstrip("/")
        if unpack_type == "archive-extract":
            self.is_unpacked_archive = True

    def finish(self):
        """finish the UrlNode"""
        self._set_status(UrlNode.FINISH)

    def add_child(self, idx, url, urldata, d):
        """add a child to the UrlNode"""
        child = UrlNode(idx, url, urldata, d, self)
        self.children.append(child)
        return child

    def get_child(self, url):
        """get a child of the UrlNode"""
        children = []
        for child in self.children:
            if child.url == url:
                children.append(child)
        if len(children) == 1:
            return children[0]
        elif len(children) == 0:
            return None
        else:
            raise UnpackTracerError(
                "Multiple UrlNode children with url %s" % url)

    def get_first_unprocessed_child(self, url):
        """get first unprocessed child of the UrlNode

        Badly written recipes or bad bbappends may introduce duplicate URLs in
        the same recipe, causing UnpackTracer.start_url() to be called multiple
        times for the same URL; to be able to handle also such corner cases, we
        need to check if the URL has already been processed and return the first
        unprocessed child UrlNode.
        """
        for child in self.children:
            if child.url == url and child.status == UrlNode.CREATE:
                return child
        return None

    def get_child_by_destdir(self, destdir):
        """
        get a child of the UrlNode by destdir.
        """
        found = []
        for child in self.children:
            if child.destdir == destdir:
                found.append(child)
        if len(found) == 1:
            return found[0]
        elif len(found) == 0:
            return None
        else:
            raise UnpackTracerError(
                "Multiple UrlNode children with destdir %s" % destdir)

    def get_toplevel_parent(self):
        """get the top-level parent of the UrlNode (excluding the root node)"""
        if self.parent and self.parent.parent is None:
            return self
        else:
            return self.parent.get_toplevel_parent()

    def process_tree_data(self, callback, *args, **kwargs):
        """
        process all nodes of the tree recursively, calling callback on each node
        """
        callback(self, *args, **kwargs)
        for child in self.children:
            child.process_tree_data(callback, *args, **kwargs)

    def delete_tree(self):
        """
        delete the tree of nodes recursively
        """
        def _delete_tree(node):
            for child in node.children:
                _delete_tree(child)
                del child
            node.children = []
        _delete_tree(self)


class LocalPathDataCache(object):
    """class used to retrieve and cache local path metadata"""
    def __init__(self):
        self.layer = Cache(get_layer)
        self.git_data = Cache(git_data)
        self.git_status = Cache(git_status)

class UnpackTracerError(Exception):
    pass

def get_fileindex(d, unpackdir=None):
    """
    get file index from index file, if it exists, otherwise create a new one
    """
    if unpackdir is None:
        unpackdir = d.getVar("WORKDIR")
    index_file = d.getVar("BBTRACER_FILEINDEX")
    if os.path.isfile(index_file):
        fi = FileIndex.read(index_file)
        if fi.rootdir != unpackdir:
            bb.fatal("bbtracer: unpackdir mismatch in fileindex")
        return fi
    else:
        return FileIndex(unpackdir)

class UnpackTracer(object):
    """
    class that traces unpacked source files back to their respective upstream
    SRC_URI entries, for software composition analysis, license compliance and
    detailed SBOM generation purposes.
    """

    def __init__(self):
        self.root = None
        self.current = None
        self.urlmap = {}
        self.fileindex = None
        self.unpackdir = None
        self.cache = LocalPathDataCache()
        self.urlcount = 0


    def start(self, unpackdir, urldata_dict, d):
        """
        Start tracing the core Fetch.unpack process, using an index to map
        unpacked files to each SRC_URI entry.
        This method is called by Fetch.unpack and it may receive nested calls by
        GitSM and NpmShrinkWrap fetchers.
        Given the nested calls, we must ensure that tree's root node and file
        index are created and initialized only once, when the first call to this
        method is received.
        """

        def start_root():
            """start root node"""

            def set_skip(skip):
                """set skip flag and write it to file, for further use by
                bbtracer_unpack_task_postfunc in bbtracer.bbclass"""
                self.skip = skip
                skip_file = d.getVar("BBTRACER_SKIPFILE")
                with open(skip_file, "w") as f:
                    f.write("1" if skip else "0")

            self.unpackdir = unpackdir.rstrip("/")
            self.root = UrlNode(self.urlcount, "root", None, d, None)
            self.urlcount += 1
            self.current = self.root

            self.json_file = d.getVar("BBTRACER_JSON")
            bb.utils.mkdirhier(os.path.dirname(self.json_file))

            self.index_file = d.getVar("BBTRACER_FILEINDEX")
            bb.utils.mkdirhier(os.path.dirname(self.index_file))

            self.skip_paths = d.getVar("BBTRACER_SKIPPATHS").split()
            self.skip_names = d.getVar("BBTRACER_SKIPNAMES").split()

            self.fileindex = get_fileindex(d, unpackdir)
            status = self.fileindex.update(
                skip_names = self.skip_names,
                skip_paths = self.skip_paths,
            )
            if any([status.added, status.modified, status.removed]):
                if os.path.isfile(self.json_file):
                    bb.debug(1, "bbtracer: unpackdir is not empty and %s is "
                        "already there, skipping unpack tracing"
                        % self.json_file)
                    set_skip(True)
                    return
                else:
                    bb.fatal("bbtracer: unpackdir is not empty, but %s is not"
                        " there, cannot trace unpacked files"
                        % self.json_file)
            else:
                set_skip(False)

            self.layers = get_layers(d)
            self.current.start()
            self.current.unpack("root", self.unpackdir)

        if self.root is None:
            start_root()
        if self.skip:
            return
        if self.current == self.root:
            # bad bbappends may introduce duplicate SRC_URI entries in recipes,
            # we need to handle them here
            src_uris = d.getVar("SRC_URI").split()
            if len(src_uris) != len(urldata_dict):
                bb.warn("bbtracer: duplicate SRC_URI entries found, we try to"
                    " handle them, but please fix your recipe")
            for src_uri in src_uris:
                if src_uri not in urldata_dict:
                    raise UnpackTracerError(
                        "SRC_URI entry %s not found in urldata_dict" % src_uri)
                self.current.add_child(
                    self.urlcount, src_uri, urldata_dict[src_uri], d)
                self.urlcount += 1
        else:
            # implicit URLs added by GitSM and NpmShrinkWrap fetchers
            for url, urldata in urldata_dict.items():
                self.current.add_child(self.urlcount, url,  urldata, d)
                self.urlcount += 1


    def start_url(self, url):
        """
        Start tracing url unpack process.
        This method is called by Fetch.unpack before the fetcher-specific unpack
        method starts, and it may receive nested calls by GitSM and NpmShrinkWrap
        fetchers.
        """
        if self.skip:
            return
        child = self.current.get_first_unprocessed_child(url)
        if child is None:
            raise UnpackTracerError("url %s not found" % url)
        self.current = child
        self.current.start()
        ud = self.current.urldata
        if ud.type in [ "git", "gitsm" ] and ud.nocheckout:
            self.current.unpack("git-bareclone", None)

    def unpack(self, unpack_type, destdir):
        """
        Set unpack_type and destdir for current url.
        This method is called by the fetcher-specific unpack method after url
        tracing started.
        """
        if self.skip:
            return
        self.current.unpack(unpack_type, destdir)


    def finish_url(self, url):
        """
        Finish tracing url unpack process and update the file index.
        This method is called by Fetch.unpack after the fetcher-specific unpack
        method finished its job, and it may receive nested calls by GitSM
        and NpmShrinkWrap fetchers.
        """

        def add_missing_npm_children(n):
            """
            create missing UrlNode children for npm "manual" deps. NpmShrinkWrap
            processes npm "auto" (i.e. git) deps through recursive calls to the
            main Fetch.unpack method, while "manual" deps are processed directly
            by NpmShrinkWrap fetcher, so we need to create children for the
            latter.
            """
            ud = n.urldata
            if ud is None:
                return
            ld = n.d.createCopy()
            flags = ld.getVarFlags("SRC_URI")
            checksum_names = tuple("%ssum" % checksum_id
                for checksum_id in bb.fetch2.CHECKSUM_LIST)
            for flag in flags:
                if flag.endswith(checksum_names):
                    ld.delVarFlag("SRC_URI", flag)
            for dep in ud.deps:
                if not dep["localpath"]:
                    continue
                # In case of local tarball and link sources, NpmShrinkWrap does
                # not set an url for the dependency, so we use the localpath
                # instead.
                if not n.get_child(dep["url"] or dep["localpath"]):
                    child = n.add_child(
                        self.urlcount,
                        url = dep["url"] or dep["localpath"],
                        urldata = bb.fetch2.FetchData(dep["url"], ld) if dep["url"] else None,
                        d = ld
                    )
                    self.urlcount += 1
                    child.start()
                    destdir = os.path.join(n.destdir, dep["destsuffix"])
                    child.unpack("npm", destdir)
                    child.finish()

        def map_deps(n):
            """map git|npm dependencies for url node n"""
            ud = n.urldata
            parent = n.parent
            if ud is None:
                return
            # is git submodule
            if ud.type == "gitsm" and parent != self.root:
                if hasattr(ud, "modpath") and parent.destdir is not None:
                    # calculate destdir for git submodule
                    n.destdir = os.path.join(
                        parent.destdir, ud.modpath).rstrip("/")
                #n.dep_of_idx = parent.idx
                n.dep_of_url = parent.url
                n.dep_of_destdir = parent.destdir
            # is npm-shrinkwrap.json file
            elif ud.type == 'npmsw':
                for dep in ud.deps:
                    # In case of local tarball and link sources, NpmShrinkWrap
                    # does not set an url for the dependency, so we use the
                    # localpath instead.
                    child = n.get_child(dep["url"] or dep["localpath"])
                    parent_destdir = re.sub(
                        "/node_modules/"+dep["name"]+"$", "", child.destdir)
                    parent_module = n.get_child_by_destdir(parent_destdir)
                    if parent_module:
                        #child.dep_of_idx = parent_module.idx
                        child.dep_of_url = parent_module.url
                        child.dep_of_destdir = parent_module.destdir
                    else:
                        # we flag child as a dependency of npm-shrinkwrap.json;
                        # this is not correct, because npm-shrinkwrap.json is
                        # not a module, but we need to do that to be able to
                        # correctly update the file index when processing
                        # npm-shrinkwrap.json url node, because we need to skip
                        # children destdirs
                        #child.dep_of_idx = n.idx
                        child.dep_of_url = n.url
                        child.dep_of_destdir = n.destdir

        def update_fileindex(n):
            """
            update the file index, skipping names in SKIP_NAMES, paths in
            self.skip_paths and (sub)paths that are dependencies of n
            """

            def get_dep_paths(n):
                """get dependency path list for url node n"""

                def _get_dep_paths(n, url, destdir, dep_paths):
                    if n.dep_of_url == url and n.dep_of_destdir == destdir:
                        dep_paths.append(n.destdir)

                dep_paths = []
                search_node = None
                if n.urldata and n.urldata.type in ["gitsm", "npmsw"]:
                    search_node = n
                elif n.parent and n.parent.urldata and n.parent.urldata.type == "npmsw":
                    # in npmsw, all dependencies (including nested ones) are
                    # fetched by one single nested Fetch.unpack call, so dep
                    # UrlNodes of the current UrlNode are all direct children of
                    # the main (parent) UrlNode (npm-shrinkwrap.json)
                    search_node = n.parent
                if search_node is not None:
                    search_node.process_tree_data(
                        _get_dep_paths, n.url, n.destdir, dep_paths)
                return dep_paths

            n.files = self.fileindex.update(
                path2update = n.destdir,
                skip_names = self.skip_names,
                skip_paths = self.skip_paths + get_dep_paths(n)
            )

        if self.skip:
            return
        if self.current.url != url:
            raise UnpackTracerError(
                "Tracer API call inconsistency:"
                " url %s does not match current url %s" % url, self.current.url)
        if self.current.parent == self.root:
            # process git and npm modules (if any) and update file index (see
            # this python module helptext)
            ud = self.current.urldata
            if ud and ud.type == "npmsw":
                add_missing_npm_children(self.current)
            self.current.process_tree_data(map_deps)
            self.current.process_tree_data(update_fileindex)
        self.current.finish()
        self.current = self.current.parent

    def complete(self):
        """check if the unpacking is complete and map urls"""

        def check_node_status(n):
            if n.status != UrlNode.FINISH:
                raise UnpackTracerError(
                    "UrlNode '%s' is not finished, cannot complete" % n.url)

        def add_checksums(n):
            """get checksums for node n"""
            ud = n.urldata
            if ud is None:
                return {}
            n.checksums = {}
            if ud.method.supports_checksum(ud):
                for checksum_id in bb.fetch2.CHECKSUM_LIST:
                    expected_checksum = getattr(ud, "%s_expected" % checksum_id)
                    if expected_checksum is None:
                        continue
                    n.checksums.update({
                        checksum_id: expected_checksum
                    })

        def add_localpath(n):
            ud = n.urldata
            if ud.type == "file":
                n.localpath = os.path.realpath(ud.localpath)
            elif ud.type == "npmsw":
                n.localpath = os.path.realpath(ud.shrinkwrap_file)
            else:
                n.localpath = None

        def add_localdir(n):
            if n.localpath:
                isdir = os.path.isdir(n.localpath)
                n.localdir = n.localpath if isdir else os.path.dirname(n.localpath)
            else:
                n.localdir = None

        def add_real_destdir(n):
            ud = n.urldata
            if ud.type in ["file", "npmsw"]:
                path_in_unpackdir = os.path.realpath(n.destdir + "/" + ud.path)
                if os.path.isdir(path_in_unpackdir):
                    n.real_destdir = path_in_unpackdir
                else:
                    n.real_destdir = os.path.dirname(path_in_unpackdir)
            else:
                n.real_destdir = n.destdir

        def add_layer(n):
            if n.localdir:
                n.layer = self.cache.layer.get(n.localdir, self.layers)
            else:
                n.layer = None

        def add_dl_loc(n):
            """add SPDX download location to node n"""
            if n.localpath:
                # get provenance data (SPDX download location) for localpath, if
                # it's in a git repo. Check if localpath has not been locally
                # changed (untracked or uncommitted) before returning provenance
                # data.
                is_dir = os.path.isdir(n.localpath)
                localdir = n.localpath if is_dir else os.path.dirname(n.localpath)
                git_data = self.cache.git_data.get(localdir)
                if git_data is None:
                    n.dl_loc = None
                    return
                relpath = os.path.relpath(n.localpath, git_data.toplevel)
                local_changes = self.cache.git_status.get(git_data.toplevel)
                if local_changes:
                    if is_dir:
                        for path in local_changes:
                            if path.startswith(relpath):
                                n.dl_loc = None
                                return
                    elif relpath in local_changes:
                        n.dl_loc = None
                        return
                path = os.path.relpath(n.localpath, git_data.toplevel)
                n.dl_loc = "git+%s@%s#%s" % (git_data.url, git_data.revision, path)
            else:
                ud = n.urldata
                if ud.type == "crate":
                    # crate fetcher converts crate:// urls to https://
                    n.dl_loc = ud.url
                else:
                    n.dl_loc = ud.type
                    if n.dl_loc == "gitsm":
                        n.dl_loc = "git"
                    proto = getattr(ud, "proto", None)
                    if proto is not None:
                        n.dl_loc += "+" + proto
                    n.dl_loc += "://" + ud.host + ud.path
                    if ud.method.supports_srcrev():
                        if ud.type == "git" and ud.nocheckout:
                            return
                        elif ud.type == "gitsm":
                            # if nocheckout=True, we don't have a checked-out
                            # revision, so we cannot add it to the download
                            # location. However, all git submodules have
                            # nocheckout=True, but they are checked out at the
                            # end of the main repo's unpack (if the main repo
                            # has ud.nocheckout=False), so in that case we need
                            # to add the revision to the download location.
                            toplevel_parent = n.get_toplevel_parent()
                            if toplevel_parent.urldata.nocheckout:
                                return
                        n.dl_loc += "@" + ud.revisions[ud.names[0]]

        def add_dep_of(n):

            def find(n, url, destdir, found):
                if found:
                    return
                if n.url == url and n.destdir == destdir:
                    found.append(n)

            found = []
            self.root.process_tree_data(
                find, n.dep_of_url, n.dep_of_destdir, found)
            n.dep_of = found[0] if found else None

        def add_src_uri(n):
            """add SRC_URI to node n"""
            toplevel_parent = n.get_toplevel_parent()
            src_uri = toplevel_parent.url
            n.src_uri = get_clean_src_uri(src_uri)
            n.unexpanded_src_uri = get_unexpanded_src_uri(src_uri, n.d)
            n.implicit_src_uri = n.url if n != toplevel_parent else None

        def add_id(n):
            # TODO: this is mainly for human readability of the json file, but
            # it's not really needed, so we could use just the existing UrlNode
            # numeric index (n.idx)
            destdir = os.path.relpath(n.real_destdir, self.unpackdir)
            md5 = hashlib.md5(
                (n.src_uri + ":" + destdir).encode("utf-8")).hexdigest()
            name = os.path.basename(n.urldata.path.rstrip("/"))
            n.id = "%s_%s_%s" % (n.idx, name, md5)

        def add_metadata_and_id(n):
            if n == self.root:
                return
            check_node_status(n)
            add_checksums(n)
            add_localpath(n)
            add_localdir(n)
            add_real_destdir(n)
            add_layer(n)
            add_dl_loc(n)
            add_dep_of(n)
            add_src_uri(n)
            add_id(n)

        def map_urls(n):
            if n == self.root:
                return
            self.urlmap[n.id] = {
                "src_uri": n.src_uri,
                "unexpanded_src_uri": n.unexpanded_src_uri,
                "implicit_src_uri": n.implicit_src_uri,
                "dl_loc": n.dl_loc,
                "destdir": os.path.relpath(n.real_destdir, self.unpackdir),
                "layer": n.layer,
                "dep_of": n.dep_of.id if n.dep_of else None,
                "checksums": n.checksums,
                "is_unpacked_archive": n.is_unpacked_archive,
                "files": {
                    "added": simple_fileindex(n.files.added, n.real_destdir),
                    "modified": simple_fileindex(n.files.modified, n.real_destdir),
                    "removed": simple_fileindex(n.files.removed, n.real_destdir)
                }
            }

        if self.skip:
            return
        if self.current != self.root:
            return False
        self.current.finish()
        self.root.process_tree_data(add_metadata_and_id)
        self.root.process_tree_data(map_urls)
        # FIXME: Maybe we should check if temp subdir exists and create it if not, to pass oe-selftest
        with open(self.json_file, "w") as f:
            json.dump(self.urlmap, f)
        self.fileindex.write(self.index_file)
        self.root.delete_tree()
        del self.root
        del self.fileindex
        return True



