#
# Copyright OpenEmbedded Contributors
#
# SPDX-License-Identifier: GPL-2.0-only

class PatchError(Exception):
    def __init__(self, msg):
        self.msg = msg

# copied and modified from openembedded-core/meta/lib/oe/patch.py
# in order to return removed file name (and not /dev/null) when mode is D(elete)
# and to return both removed and added file names in case of rename
# (Alberto Pianon <alberto@pianon.eu>, Nov 2023)
def getPatchedFiles(patchfile, striplevel, srcdir=None):
    """
    Read a patch file and determine which files it will modify.
    Params:
        patchfile: the patch file to read
        striplevel: the strip level at which the patch is going to be applied
        srcdir: optional path to join onto the patched file paths
    Returns:
        A list of tuples of file path and change mode ('A' for add,
        'D' for delete or 'M' for modify)
    """

    def patchedpath(patchline):
        filepth = patchline.split()[1]
        if filepth.endswith('/dev/null'):
            return '/dev/null'
        filesplit = filepth.split(os.sep)
        if striplevel > len(filesplit):
            bb.error('Patch %s has invalid strip level %d' % (patchfile, striplevel))
            return None
        return os.sep.join(filesplit[striplevel:])

    def abspath(path):
        if srcdir:
            return os.path.abspath(os.path.join(srcdir, path))
        else:
            return path

    for encoding in ['utf-8', 'latin-1']:
        try:
            copiedmode = False
            filelist = []
            with open(patchfile) as f:
                renamed = []
                for line in f:
                    if line.startswith('--- '):
                        patchpth = patchedpath(line)
                        if not patchpth:
                            break
                        if copiedmode:
                            addedfile = patchpth
                        else:
                            removedfile = patchpth
                    elif line.startswith('+++ '):
                        addedfile = patchedpath(line)
                        if not addedfile:
                            break
                    elif line.startswith('*** '):
                        copiedmode = True
                        removedfile = patchedpath(line)
                        if not removedfile:
                            break
                    elif line.startswith('rename from '):
                        line = "--- a/" + line.split(maxsplit=2)[2]
                        renamed_old = patchedpath(line)
                    elif line.startswith('rename to '):
                        line = "+++ b/" + line.split(maxsplit=2)[2]
                        renamed_new = patchedpath(line)
                    else:
                        removedfile = None
                        addedfile = None
                        renamed_old = None
                        renamed_new = None

                    if renamed_old and renamed_new:
                        filelist.append((abspath(renamed_old), "D"))
                        filelist.append((abspath(renamed_new), "A"))
                        renamed.append((renamed_old, renamed_new))

                    if addedfile and removedfile:
                        if (removedfile, addedfile) in renamed:
                            continue
                        if removedfile == '/dev/null':
                            filelist.append((abspath(addedfile), "A"))
                        elif addedfile == '/dev/null':
                            filelist.append((abspath(removedfile), "D"))
                        else:
                            filelist.append((abspath(addedfile), "M"))
        except UnicodeDecodeError:
            continue
        break
    else:
        raise PatchError('Unable to decode %s' % patchfile)

    return filelist

# copied and modified from openembedded-core/meta/lib/oe/recipeutils.py in order
# to use the above modified function
# (Alberto Pianon <alberto@pianon.eu>, Nov 2023)
def get_recipe_patched_files(d):
    """
    Get the list of patches for a recipe along with the files each patch modifies.
    Params:
        d: the datastore for the recipe
    Returns:
        a dict mapping patch file path to a list of tuples of changed files and
        change mode ('A' for add, 'D' for delete or 'M' for modify)
    """
    import oe.patch
    patches = oe.patch.src_patches(d, expand=False)
    patchedfiles = {}
    for patch in patches:
        _, _, patchfile, _, _, parm = bb.fetch.decodeurl(patch)
        striplevel = int(parm['striplevel'])
        patchedfiles[patchfile] = getPatchedFiles(patchfile, striplevel, os.path.join(d.getVar('S'), parm.get('patchdir', '')))
    return patchedfiles