# Copyright (C) 2023 Alberto Pianon <pianon@array.eu>
#
# SPDX-License-Identifier: GPL-2.0-only

import os
import re
import time
import csv
import tempfile

from bb.utils import sha1_file

def scandir(path, skip_names=[], paths2skip=[]):
    """
    return a sorted tree of scandir entries for files recursively found in path
    skipping entries with names in skip_names (string list) or with paths
    matching paths in paths2skip (regexp list).
    The tree is returned as a dictionary with path as keys and scandir entries
    as values.
    os.scandir is used instead of os.walk or os.listdir because it is much
    faster
    """

    def _scandir(path, tree, skip_names, paths2skip):
        with os.scandir(path) as scan:
            scan_list = [ e for e in scan ]
        for e in scan_list:
            if e.name in skip_names or any([p.match(e.path) for p in paths2skip]):
                continue
            if e.is_dir() and not e.is_symlink():
                _scandir(e.path, tree, skip_names, paths2skip)
            else:
                tree[e.path] = e

    tree = {}
    _scandir(path, tree, skip_names, paths2skip)
    paths = list(tree.keys())
    sorted_tree = {path: tree[path] for path in sorted(paths)}
    return sorted_tree

def get_stats(e):
    """
    get the stats of a scandir entry as a tuple.
    Only the relevant fields to update a file index are returned. Such fields
    are the same used by git to detect file changes.
    """
    s = os.stat(e.path, follow_symlinks=False)
    return (s.st_mode, s.st_mtime, s.st_ctime, s.st_uid, s.st_gid,
        s.st_ino, s.st_size)

def is_subpath(subpath, path):
    """
    backwards-compatible function to calculate if subpath is a subpath of path
    """
    return not os.path.relpath(subpath, path).startswith("..")

def get_filesystem_time(path):
    """
    get the current filesystem time by creating a temporary file and getting
    its modification time.
    Take as input a path, which must be a directory within the filesystem, where
    the temporary file will be created.
    Return a float.

    In Linux, filesystem timestamps are always slightly behind current time
    because they take the last timer click, which may be up to 10ms in the past,
    depending on CONFIG_HZ (see https://unix.stackexchange.com/q/759880/589996).
    Since it is based on timer click, filesystem time has an actual resolution
    which is lower than the nominal one - eg. if CONFIG_HZ=100, the actual
    resolution is 10ms even if the nominal one is 1ns.
    """
    with tempfile.NamedTemporaryFile(dir=path) as f:
        return os.stat(f.name).st_mtime


class FileIndexError(Exception):
    pass

class FileIndexEntry:
    """
    FileIndexEntry represents a file in the file index.
    Fields are the same used by git to detect file changes.
    """
    def __init__(self, link, stats, sha1):
        self.link = link
        self.stats = stats
        self.sha1 = sha1

    @staticmethod
    def _props(e, link, stats=None, sha1=None):
        """
        calculate the properties of a file index entry. Take as input a scandir
        entry, a link and optionally stats and sha1. If stats and sha1 are not
        provided, they are calculated from the scandir entry.
        """
        stats = stats or get_stats(e) if not link else None
        sha1 = sha1 or sha1_file(e.path) if not link else None
        return link, stats, sha1

    @classmethod
    def create(cls, e, link):
        """
        create a file index entry.
        Take as input a scandir entry and a link target string (None if it is
        not a link).
        Return a FileIndexEntry object.
        """
        return cls(*cls._props(e, link))

    def update(self, e, link, stats=None, sha1=None):
        """
        update a file index entry.
        Take as input a scandir entry, a link, and optionally stats and sha1. If
        stats and sha1 are not provided, they are calculated from the scandir
        entry.
        """
        self.__init__(*self._props(e, link, stats, sha1))

class FileIndexStatus:
    """
    FileIndexStatus represents the status of a file index update.
    It contains three dictionaries, one for each type of change: added, modified
    and removed files. Each dictionary has file paths as keys and FileIndexEntry
    objects as values.
    """
    def __init__(self, added, modified, removed):
        self.added = added
        self.modified = modified
        self.removed = removed


class FileIndex:
    """
    FileIndex represents a file index for a given root directory.
    It is a dictionary with file paths as keys and FileIndexEntry objects as
    values. It is used to detect file changes in a directory tree, using an
    algorithm similar to that used by git, handling also the racy git problem
    (see https://git-scm.com/docs/racy-git/en)
    """

    UNCHANGED = 0
    ADDED = 1
    MODIFIED = 2
    REMOVED = 3

    def __init__(self, rootdir, timestamp=None):
        self.rootdir = rootdir.rstrip("/")
        self.entries = {}
        self.timestamp = timestamp or get_filesystem_time(self.rootdir)

    def add_or_update_entry(self, e):
        """
        add or update a file index entry.
        Take as input a scandir entry.
        Return the status of the entry: ADDED, MODIFIED or UNCHANGED.
        """
        link = os.readlink(e.path) if e.is_symlink() else None
        if e.path in self.entries:
            entry = self.entries[e.path]
            if link:
                if link != entry.link:
                    entry.update(e, link)
                    return FileIndex.MODIFIED
                else:
                    return FileIndex.UNCHANGED
            stats = get_stats(e)
            mtime = stats[1]
            if entry.stats != stats or self.timestamp <= mtime:
                # see https://git-scm.com/docs/racy-git/en
                sha1 = sha1_file(e.path)
                if sha1 != entry.sha1:
                    entry.update(e, link, stats, sha1)
                    return FileIndex.MODIFIED
            return FileIndex.UNCHANGED
        else:
            self.entries.update(
                {e.path: FileIndexEntry.create(e, link)})
            return FileIndex.ADDED

    def remove_entry(self, path):
        """
        remove a file index entry. Take as input a file path.
        Return the status of the entry: REMOVED or UNCHANGED.
        """
        if path in self.entries:
            del self.entries[path]
            return FileIndex.REMOVED
        return FileIndex.UNCHANGED

    def update(self, path2update=None, skip_names=[], skip_paths=[]):
        """
        update the file index.
        May take as input a specific (sub)path to update, and lists of names and
        paths to skip. Wildcards (*) are accepted in skip_paths.
        Return a FileIndexStatus object containing the status of the update.
        """
        path2update = (path2update or self.rootdir).rstrip("/")
        if not is_subpath(path2update, self.rootdir):
            raise FileIndexError(
                "path %s is not a subdir of %s" % (path, self.rootdir))
        paths2skip = [
            re.compile(
                "^"
                + re.escape(p).rstrip("/").replace("\*", ".*")
                + "(/|$)"
            )
            for p in skip_paths
        ]
        tree = scandir(path2update, skip_names, paths2skip)
        added = {}
        modified = {}
        removed = {}
        to_remove = []
        for path, e in tree.items():
            res = self.add_or_update_entry(e)
            if res == FileIndex.ADDED:
                added.update({path: self.entries[path]})
            elif res == FileIndex.MODIFIED:
                modified.update({path: self.entries[path]})
        for path in self.entries.keys():
            # remove deleted files from the index
            if path in tree:
                continue
            if not is_subpath(path, path2update):
                continue
            if any([p.match(path) for p in paths2skip]):
                continue
            parts = os.path.relpath(path, path2update).split("/")
            if any([p in skip_names for p in parts]):
                continue
            removed.update({path: self.entries[path]})
            to_remove.append(path)
        for path in to_remove:
            self.remove_entry(path)
        self.timestamp = get_filesystem_time(self.rootdir)
        return FileIndexStatus(added, modified, removed)

    def write(self, path):
        """
        write the file index to a file.
        Take as input a file path.
        """
        with open(path, "w") as f:
            writer = csv.writer(f)
            writer.writerow(["rootdir", "timestamp"])
            writer.writerow([self.rootdir, self.timestamp])
            writer.writerow(["path", "link", "mode", "mtime", "ctime", "uid",
                "gid", "ino", "size", "sha1"])
            for path, entry in self.entries.items():
                if entry.stats is not None:
                    writer.writerow(
                        [path, entry.link, *entry.stats, entry.sha1])
                else:
                    writer.writerow([path, entry.link, None, None, None, None,
                        None, None, None, entry.sha1])

    @classmethod
    def read(cls, path):
        """
        read a file index from a file.
        Take as input a file path.
        Return a FileIndex object.
        """
        with open(path, "r") as f:
            reader = csv.reader(f)
            next(reader) # skip header
            row = next(reader)
            rootdir = row[0]
            timestamp = float(row[1])
            index = cls(rootdir, timestamp)
            next(reader) # skip header
            for row in reader:
                if not row:
                    # this is a workaround for a bug in the csv module which
                    # causes it to randomly return an empty row at the end of
                    # the file
                    continue
                path, link, mode, mtime, ctime, uid, gid, ino, size, sha1 = row
                stats = (int(mode), float(mtime), float(ctime), int(uid),
                    int(gid), int(ino), int(size)) if mode else None
                link = link if link else None
                sha1 = sha1 if sha1 else None
                index.entries.update(
                    {path: FileIndexEntry(link, stats, sha1)})
        return index


def simple_fileindex(entries, destdir):
    """return a simplified file index, with paths relative to destdir"""
    return {
        os.path.relpath(path, destdir):
        entry.sha1 if entry.sha1 else "->" + entry.link
        for path, entry in entries.items()
    }
