#!/usr/bin/env python3


import os
import time
import shutil
import unittest
import tempfile
from bbtracer.file_index import FileIndex, get_filesystem_time

from bb.utils import sha1_file

class TestFileIndex(unittest.TestCase):

# TODO: make unit tests independent from each other

    @classmethod
    def setUpClass(cls):
        cls.tmpdir = tempfile.mkdtemp()
        cls.fi = FileIndex(cls.tmpdir)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.tmpdir)

    def test_0_empty_file_index(self):
        status = self.fi.update()
        self.assertEqual(len(self.fi.entries), 0)
        self.assertEqual(status.added, {})
        self.assertEqual(status.removed, {})
        self.assertEqual(status.modified, {})

    def test_1_add_file(self):
        # add a file
        with open(self.tmpdir + "/file0", "w") as f:
            f.write("test")
        status = self.fi.update()
        self.assertEqual(len(self.fi.entries), 1)
        self.assertEqual(len(status.removed), 0)
        self.assertEqual(len(status.modified), 0)
        self.assertEqual(len(status.added), 1)
        self.assertEqual(list(status.added.keys())[0], self.tmpdir + "/file0")
        entry = status.added[self.tmpdir + "/file0"]
        self.assertIsNone(entry.link)
        self.assertEqual(
            entry.sha1, "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3")

    def test_3_add_link(self):
        return
        # add a link
        os.symlink(self.tmpdir + "/file0", self.tmpdir + "/link0")
        status = self.fi.update()
        self.assertEqual(len(self.fi.entries), 2)
        self.assertEqual(len(status.removed), 0)
        self.assertEqual(len(status.modified), 0)
        self.assertEqual(len(status.added), 1)
        self.assertEqual(list(status.added.keys())[0], self.tmpdir + "/link0")
        entry = status.added[self.tmpdir + "/link0"]
        self.assertEqual(entry.link, self.tmpdir + "/file0")
        self.assertEqual(entry.sha1, None)

    def test_4_racy_git(self):
        # "racy git": this modification happens in the same second as the
        # creation of the file, and the file size remains the same, so if
        # one relies only on file modification time, it will not be detected
        # in some linux filesystems having a 1s or 2s resolution for file
        # modification time.
        while True:
            # wait for the beginning of a second
            t = get_filesystem_time(self.tmpdir)
            if (t - int(t)) < 0.01:
                break
        with open(self.tmpdir + "/file1", "w") as f:
            f.write("test")
        status = self.fi.update()
        entry = status.added[self.tmpdir + "/file1"]
        self.assertEqual(
            entry.sha1, "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3")
        with open(self.tmpdir + "/file1", "w") as f:
            f.write("jdoe")
        status = self.fi.update()
        self.assertEqual(len(self.fi.entries), 2)
        self.assertEqual(len(status.added), 0)
        self.assertEqual(len(status.removed), 0)
        with open(self.tmpdir + "/file1") as f:
            content = f.read()
        self.assertEqual(content, "jdoe")
        self.assertEqual(len(status.modified), 1)
        self.assertEqual(
            list(status.modified.keys())[0], self.tmpdir + "/file1")
        entry = status.modified[self.tmpdir + "/file1"]
        self.assertIsNone(entry.link)
        self.assertEqual(
            entry.sha1, "d35514736146439b7277437016cdb40d7fb65497")

    def test_5_remove_file(self):
        os.remove(self.tmpdir + "/file1")
        status = self.fi.update()
        self.assertEqual(len(self.fi.entries), 1)
        self.assertEqual(len(status.added), 0)
        self.assertEqual(len(status.modified), 0)
        self.assertEqual(len(status.removed), 1)
        self.assertEqual(list(status.removed.keys())[0], self.tmpdir + "/file1")
        entry = status.removed[self.tmpdir + "/file1"]
        self.assertIsNone(entry.link)
        self.assertEqual(
            entry.sha1, "d35514736146439b7277437016cdb40d7fb65497")

    def test_6_remove_link(self):
        return
        os.remove(self.tmpdir + "/link0")
        status = self.fi.update()
        self.assertEqual(len(self.fi.entries), 1)
        self.assertEqual(len(status.added), 0)
        self.assertEqual(len(status.modified), 0)
        self.assertEqual(len(status.removed), 1)
        self.assertEqual(list(status.removed.keys())[0], self.tmpdir + "/link0")
        entry = status.removed[self.tmpdir + "/link0"]
        self.assertEqual(entry.link, self.tmpdir + "/file0")
        self.assertEqual(entry.sha1, None)


if __name__ == '__main__':
    unittest.main()
    # Path: lib/bbtracer/tests/test_unpack_tracer.py