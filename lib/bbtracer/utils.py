# Copyright (C) 2023 Alberto Pianon <pianon@array.eu>
#
# SPDX-License-Identifier: GPL-2.0-only

import os
import re

import bb.process
from oe.recipeutils import split_var_value

class Cache(object):
    """generic cache class"""

    def __init__(self, callback):
        self.cache = {}
        self.callback = callback

    def get(self, key, *args, **kwargs):
        """get a cache entry (or create it if it does not exist)"""
        if key not in self.cache:
            self.cache[key] = self.callback(key, *args, **kwargs)
        return self.cache[key]

def git(cmd, cwd=None):
    """run a git command and return its output"""
    stdout, _ = bb.process.run(["git"] + cmd, cwd=cwd)
    return stdout.strip()

class GitData(object):
    def __init__(self, url, branch, revision, toplevel, prefix):
        self.url = url
        self.branch = branch
        self.revision = revision
        self.toplevel = toplevel
        self.prefix = prefix

def git_data(local_dir):
    """
    get upstream data (git repo url, branch, revision, toplevel dir and prefix)
    for local_dir, if local_dir is within a git repo and if current HEAD is
    contained in a remote branch
    """
    # copied and adapted from https://git.yoctoproject.org/poky-contrib/commit/?h=jpew/spdx-downloads&id=68c80f53e8c4f5fd2548773b450716a8027d1822

    local_dir = os.path.realpath(local_dir)
    try:
        branches = git(
            ["branch", "-qr", "--format=%(refname)", "--contains", "HEAD"],
            cwd=local_dir
        )
        branches = branches.splitlines()
        branches.sort()
        current_branch = git(
            ["branch", "--no-color", "--show-current"], cwd=local_dir)

        found = False
        branch = None
        for b in branches:
            if b.startswith("refs/remotes"):
                if not b.startswith("refs/remotes/m/"):
                    # refs/remotes/m/ -> repo manifest remote, it's not a real
                    # remote (see https://stackoverflow.com/a/63483426)
                    remote = b.split("/")[2]
                    found = True
                    if b.endswith("/" + current_branch):
                        branch = current_branch
        if not found:
            # current HEAD is not found in any remote branch, so we cannot
            # conclude upstream provenance for local_dir
            return None

        url = git(["remote", "get-url", remote], cwd=local_dir)
        revision = git(["rev-parse", "HEAD"], cwd=local_dir)
        toplevel = git(["rev-parse", "--show-toplevel"], cwd=local_dir)
        prefix = git(["rev-parse", "--show-prefix"], cwd=local_dir)
        return GitData(url, branch, revision, toplevel, prefix.rstrip("/"))

    except bb.process.ExecutionError:
        return None

def git_status(git_dir):
    """get list of untracked or uncommitted files (new or modified) in
    git_dir"""
    stdout, _ = bb.process.run(["git", "status", "--porcelain"], cwd=git_dir)
    return [ line[3:] for line in stdout.rstrip().splitlines() ]

def get_layers(d):
    """get the list of layers for the current build"""
    layers = (d.getVar('BBLAYERS') or "").split()
    return {
        os.path.basename(path): os.path.realpath(path) + "/"
        for path in layers
    }

def get_layer(path, layers):
    """get the layer name for a given path"""

    for layer, layer_path in layers.items():
        if path.startswith(layer_path):
            return layer
    return None

def get_unexpanded_src_uri(src_uri, d):
    """get unexpanded src_uri"""
    src_uris = d.getVar("SRC_URI").split() if d.getVar("SRC_URI") else []
    if src_uri not in src_uris:
        return None
    unexp_src_uris = split_var_value(
        d.getVar("SRC_URI", expand=False), assignment=False)
    for unexp_src_uri in unexp_src_uris:
        if src_uri in d.expand(unexp_src_uri).split():
            # some unexpanded src_uri with expressions may expand to multiple
            # lines/src_uris
            return unexp_src_uri
    return src_uri

clean_src_uri_regex = [
    r"(?<=://)/[^;]+$",     # url path (as in file:/// or npmsw:///)
    r"(?<=://)/[^;]+(?=;)", # url path followed by param
    r"(?<==)/[^;]+$",       # path in param
    r"(?<==)/[^;]+(?=;)",   # path in param followed by another param
]
clean_src_uri_regex = [ re.compile(r) for r in clean_src_uri_regex ]

def get_clean_src_uri(src_uri):
    """clean expanded src_uri from possible local absolute paths"""
    for r in clean_src_uri_regex:
        src_uri = r.sub("<local-path>", src_uri)
    return src_uri



