# Copyright (C) 2023 Alberto Pianon <pianon@array.eu>
#
# SPDX-License-Identifier: GPL-2.0-only

BBTRACER_DIR = "${TMPDIR}/bbtracer/${MULTIMACH_TARGET_SYS}/${PN}/${EXTENDPE}${PV}-${PR}"
BBTRACER_WORKDIR = "${WORKDIR}/bbtracer"

BBTRACER_JSON = "${BBTRACER_WORKDIR}/do_${BB_CURRENTTASK}.json"
BBTRACER_SKIPFILE = "${T}/bbtracer.do_${BB_CURRENTTASK}.skip"
BBTRACER_FILEINDEX = "${T}/bbtracer.fileindex.csv"

# file and dir names skipped by bbtracer fileindex
BBTRACER_SKIPNAMES = ".git .svn .hg .bzr .cvs"
# paths skipped by bbtracer fileindex

def bbtracer_skippaths_patches(d):
    import os
    import bb.fetch2
    s = d.getVar("S")
    patchdirs = set()
    patchdirs.add(s)
    for u in d.getVar("SRC_URI").split():
        _, _, _, _, _, parm = bb.fetch2.decodeurl(u)
        if "patchdir" in parm:
            patchdirs.add(
                os.path.normpath(
                    os.path.join(s, parm["patchdir"])
                )
            )
    names2skip = [".pc", "patches"]
    paths2skip = []
    for patchdir in patchdirs:
        for name in names2skip:
            paths2skip.append(os.path.join(patchdir, name))
    return " ".join(paths2skip)

# paths skipped by bbtracer fileindex
BBTRACER_SKIPPATHS = " \
    ${BBTRACER_WORKDIR} \
    ${T} \
    ${RECIPE_SYSROOT} \
    ${RECIPE_SYSROOT_NATIVE} \
    ${SYSROOT_DESTDIR} \
    ${D} \
    ${PSEUDO_LOCALSTATEDIR} \
    ${WORKDIR}/sstate-build-* \
    ${PKGD} \
    ${PKGDEST} \
    ${SDE_DIR} \
    ${SDE_DEPLOYDIR} \
    ${LICSSTATEDIR} \
    ${SPDXDIR} \
    ${@bbtracer_skippaths_patches(d)} \
"

# Tasks that may write files to WORKDIR (outside BBTRACER_SKIPPATHS) and that
# may be executed before or in parallel with other tasks writing files to
# WORKDIR (including, but not limited to, do_unpack) must be added to
# BBTRACER_EXTRATASKS so they can be correctly handled by bbtracer and race
# conditions can be avoided

BBTRACER_EXTRATASKS = " \
    do_write_config \
    do_configure \
    do_generate_toolchain_file \
    do_cargo_setup_snapshot \
    do_rust_setup_snapshot \
    do_rust_gen_targets \
    do_rust_create_wrappers \
"

def get_and_update_fileindex(d):
    from bbtracer import get_fileindex
    fi = get_fileindex(d)
    status = fi.update(
        skip_paths=d.getVar("BBTRACER_SKIPPATHS").split(),
        skip_names=d.getVar("BBTRACER_SKIPNAMES").split()
    )
    fi.write(d.getVar("BBTRACER_FILEINDEX"))
    return status, fi

def load_json(d):
    import os
    import json
    json_file = d.getVar("BBTRACER_JSON")
    if os.path.isfile(json_file):
        with open(json_file) as f:
            return json.load(f)
    return {}

def save_json(data, d):
    import json
    json_file = d.getVar("BBTRACER_JSON")
    bb.utils.mkdirhier(os.path.dirname(json_file))
    with open(json_file, "w") as f:
        json.dump(data, f)

def get_files(status, fi):
    from bbtracer import simple_fileindex
    return {
        "added": simple_fileindex(status.added, fi.rootdir),
        "modified": simple_fileindex(status.modified, fi.rootdir),
        "removed": simple_fileindex(status.removed, fi.rootdir)
    }

def skip(d):
    import os
    skip_file = d.getVar("BBTRACER_SKIPFILE")
    if os.path.exists(skip_file):
        with open(skip_file) as f:
            if f.read() == "1":
                return True
    return False

def set_skip(d):
    import os
    skip_file = d.getVar("BBTRACER_SKIPFILE")
    bb.utils.mkdirhier(os.path.dirname(skip_file))
    with open(skip_file, "w") as f:
        f.write("1")

def task_exists(task, d):
    tasks = list(filter(lambda k: d.getVarFlag(k, "task"), d.keys()))
    return task in tasks

def add_deps(task, deps, d):
    existing = d.getVarFlag(task, "deps", False) or []
    for dep in deps:
        if not task_exists(dep, d):
            continue
        if dep not in existing:
            existing.append(dep)
    d.setVarFlag(task, "deps", existing)

def get_deps(task, d):
    """recursive function to get all task dependencies"""
    deps = set()
    for dep in d.getVarFlag(task, "deps", False) or []:
        if task_exists(dep, d):
            deps.add(dep)
            deps.update(get_deps(dep, d))
    return deps

def add2varflag(var, flag, value, mode, d):
    existing = d.getVarFlag(var, flag, False) or ""
    if value not in existing.split():
        if mode == "append":
            value = existing + " " + value
        elif mode == "prepend":
            value = value + " " + existing
        d.setVarFlag(var, flag, value)


python bbtracer_generic_prefunc () {
    import os
    task = d.getVar("BB_CURRENTTASK")
    if os.path.exists(d.getVar("BBTRACER_JSON")):
        bb.debug(
            1, "bbtracer: found bbtracer data, skipping do_%s prefunc" % task)
        set_skip(d)
        return
    status, fi = get_and_update_fileindex(d)
    if any([status.added, status.modified, status.removed]):
        bb.warn("Some files have been added, modified or removed before do_%s,"
            "tracing them as '<unknown-pre-do_%s>'" % (task, task))
        data = {"<unknown-pre-do_%s>" % task: {"files": get_files(status, fi)}}
        save_json(data, d)
}

python bbtracer_generic_postfunc () {
    task = d.getVar("BB_CURRENTTASK")
    if skip(d):
        bb.debug(1, "bbtracer: skipping do_%s postfunc" % task)
        return
    status, fi = get_and_update_fileindex(d)
    if any([status.added, status.modified, status.removed]):
        bb.debug(1, "bbtracer: found added, modified or removed files by do_%s,"
        " tracing them" % task)
        data = load_json(d)
        data.update({"do_%s" % task: {"files": get_files(status, fi)}})
        save_json(data, d)
}

python bbtracer_unpack_postfunc () {
    import os
    src_uris = d.getVar("SRC_URI").split()
    if not src_uris:
        bb.debug(1, "bbtracer: no SRC_URI entries found, skipping"
            " bbtracer_unpack_postfunc")
        return
    if skip(d):
        bb.debug(1, "bbtracer: skipping do_unpack postfunc")
        return
    status, fi = get_and_update_fileindex(d)
    if any([status.added, status.modified, status.removed]):
        bb.warn("bbtracer: some files have been added, modified or removed"
            " after bitbake's core unpack function execution, likely due to a"
            " custom do_unpack task; adding them to %s" %
            d.getVar("BBTRACER_JSON"))
        data = load_json(d)
        data["<unknown-post-do_unpack>"] = {
            "src_uri": None,
            "unexpanded_src_uri": None,
            "implicit_src_uri": None,
            "dl_loc": "NOASSERTION",
            "destdir": ".",
            "layer": None,
            "dep_of": None,
            "checksums": {},
            "is_unpacked_archive": None,
            "files": get_files(status, fi)
        }
        save_json(data, d)
}

python bbtracer_patch_postfunc () {
    """handle files added, modified or removed in workdir by do_patch"""
    if skip(d):
        bb.debug(1, "bbtracer: skipping do_patch postfunc")
        return
    import os
    import oe.recipeutils
    from bbtracer import get_recipe_patched_files
    status, fi = get_and_update_fileindex(d)
    if any([status.added, status.modified, status.removed]):
        files = get_files(status, fi)
        patchmap = load_json(d)
        found = set()
        last_patch = {}
        mode_names = {"A": "added", "M": "modified", "D": "removed"}
        local_files = oe.recipeutils.get_recipe_local_files(d, patches=True)
        patch_info = get_recipe_patched_files(d)
        for patch_localfile, patched_files in patch_info.items():
            for patch, localfile in local_files.items():
                if localfile == patch_localfile:
                    break
            else:
                bb.fatal("bbtracer: can't find corresponding patch file in"
                    " WORKDIR for localfile %s" % patch_localfile)
            pfiles = {"added": {}, "modified": {}, "removed": {} }
            patch_abspath = os.path.join(fi.rootdir, patch)
            patchmap[patch] = {
                "sha1": fi.entries[patch_abspath].sha1,
                "link": fi.entries[patch_abspath].link,
                "files": pfiles
            }
            for patched_file, mode in patched_files:
                patched_file = os.path.relpath(patched_file, fi.rootdir)
                mode_name = mode_names[mode]
                pfiles[mode_name][patched_file] = None
                last_patch[patched_file] = patch
        for patch in patchmap:
            if patch == "<unknown-pre-do_patch>":
                continue
            for mode_name in patchmap[patch]["files"]:
                for patched_file in patchmap[patch]["files"][mode_name]:
                    if last_patch[patched_file] == patch:
                        if patched_file in files[mode_name]:
                            sha1 = files[mode_name][patched_file]
                            found.add((mode_name, patched_file))
                        elif mode_name == "modified":
                            # corner case in which a file has been added by
                            # a patch and then modified by other patch(es):
                            # we don't trace single patches, so in the
                            # fileindex status the file is traced as "added"
                            sha1 = files["added"][patched_file]
                            found.add(("added", patched_file))
                        elif mode_name == "deleted":
                            # corner case in which a file has been added by
                            # a patch and then deleted by another patch: we
                            # cannot trace it in fileindex
                            sha1 = None
                        else:
                            bb.fatal("bbtracer: file %s, added by patch %s,"
                                " not found in file index" %
                                (patched_file, patch))
                        patchmap[patch]["files"][mode_name][patched_file] = sha1
        for mode_name, relpath in found:
            del files[mode_name][relpath]
        if any([files["added"], files["modified"], files["removed"]]):

            bb.warn("There are some added, modified or removed files that do"
            " not appear in patched file list, tracing them as"
            " '<unknown-do_patch>'")
            patchmap["<unknown-do_patch>"] = {
                "sha1": None, "link": None, "files": files
            }
        save_json(patchmap, d)
}

# not adding any prefunc to do_unpack to handle files already added to workdir
# before do_unpack, because we assume that WORKDIR *must* be clean (apart from
# SKIPPATHS) before running do_unpack
do_unpack[postfuncs] += "bbtracer_unpack_postfunc"

do_patch[prefuncs] =+ "bbtracer_generic_prefunc"
do_patch[postfuncs] += "bbtracer_patch_postfunc"

python do_bbtracer () {
    """dummy task to "force" sstate to save bbtracer data"""
    if not task_exists('do_unpack', d):
        bb.debug(
            1, "bbtracer: do_unpack task not found, skipping do_bbtracer")
        return
    src_uris = d.getVar("SRC_URI").split()
    if not src_uris:
        bb.debug(
            1, "bbtracer: no SRC_URI entries found, skipping do_bbtracer")
        return
    import os
    if not os.path.isdir(d.getVar("BBTRACER_WORKDIR")):
        bb.fatal(
            "bbtracer: do_bbtracer called but no bbtracer data found")
}

addtask do_bbtracer after do_unpack do_patch before do_package

SSTATETASKS += "do_bbtracer"
do_bbtracer[sstate-inputdirs] = "${BBTRACER_WORKDIR}"
do_bbtracer[sstate-outputdirs] = "${BBTRACER_DIR}"

python do_bbtracer_setscene () {
    sstate_setscene(d)
}

addtask do_bbtracer_setscene

do_bbtracer[dirs] = "${BBTRACER_WORKDIR}"


python () {
    try:
        from bb.fetch2 import DummyUnpackTracer
    except ImportError:
        bb.fatal("bbtracer: bitbake version does not support unpack tracing,"
                " please upgrade")
    extra_tasks = [
            task for task in d.getVar("BBTRACER_EXTRATASKS").split()
            if task_exists(task, d)
    ]
    for task in extra_tasks:
        add2varflag(task, "prefuncs", "bbtracer_generic_prefunc", "prepend", d)
        add2varflag(task, "postfuncs", "bbtracer_generic_postfunc", "append", d)
        # make sure that each extra task writing files to WORKDIR (excluding
        # BBTRACER_SKIPPATHS) is executed after do_unpack and do_patch and not
        # before or in parallel
        add_deps(task, ["do_unpack", "do_patch"], d)
        interdeps = []
        for task2 in extra_tasks:
            if task2 == task:
                continue
            if task in get_deps(task2, d) or task2 in get_deps(task, d):
                continue
            interdeps.append(task2)
        if interdeps:
            add_deps(task, interdeps, d)
    add_deps("do_bbtracer", extra_tasks, d)
}

# backwards compatibility code for bitbake < 2.4, not supporting addpylib
def add_bbtracer_lib_dir_to_syspath(d):
    import sys
    for p in d.getVar("BBPATH").split(":"):
        # LAYERDIR is still undefined at this stage, and __file__ doesn't
        # work here, so we need to get bbtracer layer dir from BBPATH
        if os.path.basename(p.rstrip("/")) == "meta-bbtracer":
            libdir = os.path.join(p, "lib")
            break
    if libdir not in sys.path:
        sys.path.append(libdir)
    return ""

# We need to add bbtracer lib dir early (before INHERITs get added)
BBTRACER_LIB_DIR_ADDED_TO_SYSPATH := "${@add_bbtracer_lib_dir_to_syspath(d)}"
