<!--
Copyright (C) 2023 Alberto Pianon <pianon@array.eu>

SPDX-License-Identifier: MIT
-->

# meta-bbtracer

This layer provides UnpackTracer python class implementing bitbake's API for tracing source files unpacked by bb.fetch2.Fetch.unpack back to their respective upstream SRC_URI entries, for software composition analysis, license compliance and detailed SBOM generation purposes.
The goal is to generate a set of json files tracing the whole process:

a. fetched upstream source packages

b. recipe workdir "mixed" sources

c. applied patches

d. files generates by configure task(s)

e. source files actually used to generate each binary file

For now, current (WIP) implementation covers points a-d. 

- [1. Logic](#1-logic)
  - [1.1. Handling GitSM fetcher](#11-handling-gitsm-fetcher)
  - [1.2. Handling NpmShrinkWrap fetcher](#12-handling-npmshrinkwrap-fetcher)
- [2. Adding the meta-bbtracer layer to your build](#2-adding-the-meta-bbtracer-layer-to-your-build)
- [3. Unit tests](#3-unit-tests)
- [4. Manual integration tests](#4-manual-integration-tests)
- [5. TODO](#5-todo)

# 1. Logic

For the tracing of unpacked URLs, a tree data structure is being used to trace unpacked URLs in order to be able to handle any kind of fetchers, including GitSM and NpmShrinkWrap fetchers, that expand SRC_URI entries by adding implicit URLs and by recursively calling Fetch.unpack from new (nested) Fetch instances.

For the tracing of unpacked files, a file index is being used to detect changes (added, modified, removed files) in the unpackdir. We update the file index for each unpack process, except for GitSM and NpmShrinkWrap expanded/implicit URLs, which need to be handled only at the end of the main URL's unpack process (see below for more details). To do so, we need to calculate the destdir for each GitSM and NpmShrinkWrap expanded/implicit URL, in order to update the file index separately for each URL.

Finally, we also collect dependency information for each git submodule and npm module, because such information is also needed for software composition analysis, license compliance and SBOM generation purposes.

## 1.1. Handling GitSM fetcher

GitSM fetcher recursively creates nested Fetch instances in case of nested git submodules, but it checks out only the main/top repository, while all (nested) submodules are just bare-cloned through nested Fetch.unpack calls, and checked out all at once only at the end, through a single command call within the main repo's dir (git submodule update --recursive --no-fetch).

This means that when nested Fetch.unpack calls are made, the files we need to trace are not there yet because submodules are not checked out at that point.

Therefore we need to update the file index for each submodule only at the end of the main repo's unpack process. To be able to do so, we need to calculate each submodule checkout dir to update the index only for that dir; this is done by using the modpath attribute of the GitSM urldata.

## 1.2. Handling NpmShrinkWrap fetcher

NpmShrinkWrap fetcher does not create multiple nested Fetch instances as GitSM does, but it unpacks all npm modules (including recursive dependencies) through a single nested Fetch.unpack call.

However, this is true only for "auto" (i.e. git) dependencies, while "manual" dependencies (which means most dependencies) are unpacked directly by NpmShrinkWrap fetcher without calling Fetch.unpack, so we need to update the file index for each "manual" dependency at the end of the unpack process for the main URL (which in the case of NpmShrinkWrap is not a module but a npm-shrinkwrap.json file).

However, to avoid using different code paths to handle "auto" and "manual" dependencies, we do the same also for "auto" dependencies; in this way we use the same code path to process all modules, both in GitSM and in NpmShrinkWrap.

As in GitSM, we need to calculate each npm module's destdir to update the index only for that dir; this is done by using "destsuffix" and "name" found in urldata.dep.

# 2. Adding the meta-bbtracer layer to your build

Run `bitbake-layers add-layer meta-bbtracer`

# 3. Unit tests

Only unit tests for file_index module are available.

To run them, do:

```
export PYTHONPATH=<path-to-bitbake>/lib:<path-to-meta-bbtracer>/lib
<path-to-meta-bbtracer>/lib/bbtracer/tests/test_file_index.py
```

# 4. Manual integration tests

Clone poky, meta-openembedded, meta-freescale and meta-iot-cloud repos and this repo. Make sure that the downloaded bitbake revision supports the new unpack tracer API. Init build environment, patch ot-br-posix recipe SRCREV (to turn it into a test case for nested git submodules), add layers and, if you are in a position to do so, accept FSL EULA (but read FSL EULA first!)

```
git clone git://git.yoctoproject.org/poky
git clone git://git.openembedded.org/meta-openembedded
git clone https://github.com/intel-iot-devkit/meta-iot-cloud.git
git clone git://git.yoctoproject.org/meta-freescale
git clone https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/next/meta-bbtracer
sed -i -E 's/^SRCREV = "[^"]+"$/SRCREV = "083fb6921903441bf44f46c263c123eb4af6e4a9"/' \
meta-openembedded/meta-networking/recipes-connectivity/openthread/ot-br-posix_git.bb
cd poky
. ./oe-init-build-env
cd ../..
bitbake-layers add-layer \
  meta-openembedded/meta-oe \
  meta-openembedded/meta-python \
  meta-openembedded/meta-networking \
  meta-iot-cloud \
  meta-freescale \
  meta-bbtracer
cd -
echo 'ACCEPT_FSL_EULA = "1"' >> conf/local.conf
```

Suggested test cases are:

  - bzip2: local files added to upstream source
  - snappy: git submodules
  - ot-br-posix_git@083fb692: nested git submodules
  - node-red: npm shrinkwrap
  - connman-gnome: file uri pointing to a directory
  - autoconf: file uri with a subdir in the path
  - libxml-parser-perl: duplicate SRC_URI entry (added by a bbappend)
  - qemuwrapper-cross: no SRC_URI entries
  - firmware-imx: custom do_unpack task, that unpacks files after bitbake's core unpack function execution
  - glib-2.0-native: files added to unpackdir by write_config task (meson.bbclass) before do_unpack is run (so unpackdir is not empty when do_unpack task starts)

Suggested test cases for do_patch tracing:

  - v86d: patch that deletes a file
  - patch-native: patch that adds a new file, and then other patches that modify it (patch-native is in ASSUME_PROVIDED so it is not possible to run any bitbake command directly, but running do_patch on any other recipe should trigger its building)

To test them, run:

```
bitbake -c traceunpack bzip2 snappy ot-br-posix node-red connman-gnome \
    autoconf libxml-parser-perl qemuwrapper-cross

MACHINE="imx8qm-mek" bitbake -c traceunpack firmware-imx

bitbake -c configure glib-2.0-native

bitbake -c patch v86d
bitbake -c patch acl
```

(MACHINE may be any machine supported by firmware-imx)

and check results with:

```
jq -C . tmp/bbtracer/<arch>/<PN>/<PV>-<PR>/do_unpack.json | less -R
```

and

```
jq -C . tmp/work/<arch>/<PN>/<PV>-<PR>/bbtracer/do_patch.json | less -R
```

# 5. TODO

- [ ] tests
  - [x] add unit tests for file_index
  - [ ] find a better test case for nested git submodules; add other test
  cases
  - [ ] implement automated tests against specific revisions of bitbake and openmbedded
- [ ] handle cargo/crate dependencies?
- [x] store artifacts in sstate cache
  - [ ] move it at the end
- [x] add check after do_unpack in case of custom do_unpack functions
- [x] add tracing for do_patch
- [x] add generic tracing for all other tasks
- [ ] add tracing to map debug sources to upstream sources
- [ ] integration with aliens4friends
- [ ] integration with create-spdx.bbclass
